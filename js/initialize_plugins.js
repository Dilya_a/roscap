

//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var styler = $(".styler"),
			phone = $(".phone"),
			popup = $("[data-popup]"),
			superfish = $(".superfish"),
			windowW = $(window).width(),
			windowH = $(window).height();


			
			
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(phone.length){
					include('plugins/maskedInput.js');
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}



			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler({
							// selectSmartPositioning: true
						});
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */



			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal();
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */

			/* ------------------------------------------------
			MASKEDINPUT START
			------------------------------------------------ */

					if(phone.length){
				        // $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
				        $(".phone").mask("+7(999) 999-99-99");
					}

			/* ------------------------------------------------
			MASKEDINPUT END
			------------------------------------------------ */




		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
