//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/
		

		include("js/initialize_plugins.js");

		function include(url){ 

				document.write('<script src="'+ url + '"></script>'); 

		}


//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/

$(document).ready(function(){




		/* ------------------------------------------------
		ANCHOR START
		------------------------------------------------ */
				 
			    function goUp(){
					var windowHeight = $(window).height(),
						windowScroll = $(window).scrollTop();

					if(windowScroll>windowHeight/2){
						$('.arrow_up').addClass('active');
					}

					else{
						$('.arrow_up').removeClass('active');
					}

			    }

			    goUp();
				$(window).on('scroll',goUp);

				$('.arrow_up').on('click ontouchstart',function () {

					if($.browser.safari){
						$('body').animate( { scrollTop: 0 }, 1100 );
					}
					else{
						$('html,body').animate( { scrollTop: 0}, 1100 );
					}
					return false;
					
				});

		/* ------------------------------------------------
		ANCHOR END
		------------------------------------------------ */

		function checkEmail(currInput)
	    {
	      var pattern=/^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,4}$/;
	      if(!pattern.exec($(currInput).val()))
	      {
	        return false;
	      }
	      else
	      {
	        return true;
	      }
	    }
	    function checkName(currInput)
	    {
	      var pattern=/^[а-яА-ЯёЁa-zA-Z]+$/;
	      if(!pattern.exec($(currInput).val()))
	      {
	        return false;
	      }
	      else
	      {
	        return true;
	      }
	    }

	    function checkPhone(currInput){
	    	var pattern=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
			if(!pattern.exec($(currInput).val()))
			{
			return false;
			}
			else
			{
			return true;
			}
	    }

	    function empty(currInput) {
	    	if ($(currInput).val()){
	    		return true;
	    	}
	    	else{
	    		return false;
	    	}
	    }

		$(".simple_form").on("submit", function(event){
			
			var currentForm = $(this),
				errors      = false,
				name        = currentForm.find('[name="name"]'),
				phone       = currentForm.find('[name="phone"]'),
				city		= currentForm.find('[name="city"]');

			if(!empty(name) && !checkName(name)){
				name.addClass("invalid");
				errors = true;
			}
			else{
				name.removeClass("invalid");
				errors = false;
			}

			if(!empty(phone)){
				phone.addClass("invalid");
				errors = true;
			}
			else{
				phone.removeClass("invalid");
				errors = false;
			}

			if(city.find("option:selected").index() == 0){
				city.closest('.jqselect').addClass("invalid");
				errors = true;
			}
			else{
				city.closest('.jqselect').removeClass("invalid");
				errors = false;
			}

			if(!errors){
                var A = {
                  action : 'send2',
                  Name 	: 	name.val(),
                  Phone : 	phone.val() ,
                  City 	: 	city.val()
                }

                $.post('sendmail1.php' , A , function(data){

                    currentForm.find(".form_inner").hide().siblings('.success_message').show();

                    
                });
              }

			event.preventDefault();
		})


		var applicationForm = $("#application"),
			applicationErrors = false;

		if(applicationForm.length){

			applicationForm.find('.required').on("focusout", function(){
				if(!$(this).val()){
					$(this).addClass("invalid");
					applicationErrors = true;
				}
				else{
					$(this).removeClass("invalid");
					applicationErrors = false;
				}
			})

			applicationForm.find('.required_select').on("change", function(){
				if($(this).find("option:selected").index() == 0){
					$(this).closest(".jqselect").addClass('invalid');
					applicationErrors = true;
				}
				else{
					$(this).closest(".jqselect").removeClass('invalid');
					applicationErrors = false;
				}
			})

			applicationForm.on("submit", function(event){
				event.preventDefault();
				applicationForm.find(".required").each(function(){
					if(!$(this).val()){
						applicationErrors = true;
						$(this).addClass("invalid");
					}
					else{
						applicationErrors = false;
						$(this).removeClass("invalid");
					}
				})

				applicationForm.find(".required_select").each(function(){
					if($(this).find("option:selected").index() == 0){
						$(this).closest(".jqselect").addClass('invalid');
						applicationErrors = true;
					}
					else{
						$(this).closest(".jqselect").removeClass('invalid');
						applicationErrors = false;
					}
				})

				var fName 		= applicationForm.find('[name="fName"]'),
					lName 		= applicationForm.find('[name="lName"]'),
					pName 		= applicationForm.find('[name="pName"]'),
					citizen 	= applicationForm.find('[name="citizenship"]'),
					birthDay 	= applicationForm.find('[name="birthDay"]'),
					birthMonth 	= applicationForm.find('[name="birthMonth"]'),
					birthYear 	= applicationForm.find('[name="birthYear"]')
					city 		= applicationForm.find('[name="city"]'),
					factAddress = applicationForm.find('[name="factLivingAddress"]'),
					phone 		= applicationForm.find('[name="phone"]'),
					email  		= applicationForm.find('[name="email"]'),
					goodTime	= applicationForm.find('[name="goodTime"]'),
					kreditSum	= applicationForm.find('[name="sum"]'),
					kreditLength= applicationForm.find('[name="credit_term"]'),
					experience  = applicationForm.find('input[name="experience"]'),
					stadingWork = applicationForm.find('[name="stadingWork"]');


				if(!checkName(fName)){
					applicationErrors = true;
					fName.addClass("invalid");
				}
				else{
					applicationErrors = false;
					fName.removeClass("invalid");
				}

				if(!checkName(lName)){
					applicationErrors = true;
					lName.addClass("invalid");
				}
				else{
					applicationErrors = false;
					lName.removeClass("invalid");
				}

				if (!checkEmail(email)){
					applicationErrors = true;
					email.addClass('invalid');
				}
				else{
					applicationErrors = false;
					email.removeClass('invalid');
				}


				if(!applicationErrors){
	                var B = {
	                  action 	: 'send',
	                  fName 	: fName.val(),
	                  lName		: lName.val(),
	                  pName 	: pName.val(),
	                  citizen 	: citizen.val(),
	                  city 		: city.val(),
	                  phone 	: phone.val(),
	                  goodTime 	: goodTime.val(),
	                  email 	: email.val(),
	                  livingAddress : factAddress.val(),
	                  stadingWork : stadingWork.val(),
	                  experience : experience.val(),
	                  birthDay 	: birthDay.val(),
	                  birthMonth: birthMonth.val(),
	                  birthYear : birthYear.val(),
	                  kreditSum : kreditSum.val(),
	                  kreditTerm : kreditLength.val()
	                }

	                $.post('sendmail.php' , B , function(data){
	                	location="thanks.html";
	                });
	              }
			})
		}



		/* ------------------------------------------------
		RESPONSIVE MENU START
		------------------------------------------------ */

				$(".resp_btn, .close_resp_menu").on("click ontouchstart", function(){
					$("body").toggleClass("show_menu")
				});

				$(document).on("click ontouchstart", function(event) {
			      if ($(event.target).closest("nav,.resp_btn").length) return;
			      $("body").removeClass("show_menu");
			      if(windowW <= 991){
			      	$(".menu_item").removeClass("active").find(".dropdown-menu").css("display","none");
			      }
			      event.stopPropagation();
			    });

				// проверка на наличие элемента и вставка хтмл кода
			 	//  if($(window).width() <= 767){
				// 	if ($(".menu_item").length){
				//         $(".menu_item").has('.dropdown-menu').append('<span class="touch-button"><i class="fa fa-angle-down"></i></span>');
				//     }
				// }
				// проверка на наличие элемента и вставка хтмл кода


				$('.menu_link').on('click ontouchstart',function(event){
					if($("html").hasClass("md_no-touch"))return;

			        var windowWidth = $(window).width(),
			            $parent = $(this).parent('.menu_item');
			        if(windowWidth > 991){
			          // if($("html").hasClass("md_touch")){
			            if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			              event.preventDefault();

			              $parent.toggleClass('active')
			               .siblings()
			               .find('.menu_link')
			               .removeClass('active');
			            }
			          // }  
			        }
			        
			        else{
			            
			          if((!$parent.hasClass('active')) && $parent.find('.dropdown-menu').length){

			            event.preventDefault();

			            $parent.toggleClass('active')
			             .siblings()
			             .removeClass('active');
			            $parent.find(".dropdown-menu")
			             .slideToggle()
			             .parents('.menu_item')
			             .siblings()
			             .find(".dropdown-menu")
			             .slideUp();
			          }
			        }

			    });



					// $('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link').on('click ontouchstart',function(event){
			  //       	if($("html").hasClass("md_no-touch"))return;  

			  //           var windowWidth = $(window).width(),
			  //               $parent = $(this).parent('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it');
			  //           if(windowWidth > 767){
			  //             // if($("html").hasClass("md_touch")){
			  //               if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //                 event.preventDefault();

			  //                 $parent.toggleClass('active')
			  //                  .siblings()
			  //                  .find('.categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link')
			  //                  .removeClass('active');
			  //               }
			  //             // }  
			  //           }
			            
			  //           else{
			                
			  //             if((!$parent.hasClass('active')) && $parent.find('[class*="subcategories_show"]').length){

			  //               event.preventDefault();

			  //               $parent.toggleClass('active')
			  //                .siblings()
			  //                .removeClass('active');
			  //               $parent.find("[class*='subcategories_show']")
			  //                .slideToggle()
			  //                .parents('.menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it')
			  //                .siblings()
			  //                .find("[class*='subcategories_show']")
			  //                .slideUp();
			  //             }
			  //           }

			  //       });


			  //       $(document).on("click ontouchstart", function(event) {
			  //         if ($(event.target).closest(".categories_lk , .sale_primary_nav__list > li > a, .sale_primary_nav__list_link").length) return;
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").removeClass("active");
			  //         $(".menu_categories > li, .sale_primary_nav__list > li, .menu_categories_item, .sale_primary_nav__list_it").find("[class*='subcategories_show']").css("display","none");
			  //         event.stopPropagation();
			  //       });

		/* ------------------------------------------------
		RESPONSIVE MENU END
		------------------------------------------------ */




		
});